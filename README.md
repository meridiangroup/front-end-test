#Prueba FrontEnd

La siguiente es una prueba para evaluar conocimientos de HTML, CSS, JavaScript y diseño Responsive.

Se busca que cumpla con los requerimientos especificados dentro del tiempo límite para esta prueba que es de 2 horas.

Se evaluará habilidad en HTML y CSS, además aspectos de organización de código, buenas prácticas, priorización y creatividad para resolver los requerimientos.

##Ejercicio

Clonar este repositorio.

Maquetación: Deberás traducir el diseño ubicado en [assets/Design PSD/](https://bitbucket.org/meridiangroup/front-end-test/src/88a9f580bd1a/assets/Design%20PSD/?at=master) a HTML+CSS. Si es necesario puede descargar el PSD que esta en la carpeta [assets/Design PSD/](https://bitbucket.org/meridiangroup/front-end-test/src/88a9f580bd1a/assets/Design%20PSD/?at=master)

Requerimientos: Traducir diseño a HTML+CSS. Deberás usar técnicas CSS3 y HTML semántico cuando lo requiera. Además debe realizar el sitio responsive. Se recomienda fuertemente realizar al menos la maquetación para desktop y mobile. Utilizar FontAwesome donde sea necesario.

JavaScript: Se deben realizar las siguiente tareas:

Validaciones
Marca del auto: requerido

Modelo del auto: requerido

Email: requerido

Año auto: requerido

SEO Friendly: Meridian busca siempre tener una buena posición en ránkings de búsqueda. Crear etiquetas necesarias para un buen SEO (hint: use las keywords: crédito automotriz, comparador crédito automotriz). ¿Crees que se requieran cambios en la maqueta? ¿Cuáles? Opcional: agregar share buttons y etiquetas para redes sociales (hint: http://ogp.me).

Advanced CSS: Puede usar frameworks a elección para escribir CSS teniendo en cuenta la compatibilidad con distintos browsers (hint opcional: Usar BrowserStack para chequear el renderizado en distintos navegadores). Opcional: ¿cuál sería tu enfoque en la construcción del diseño?

Para finalizar la tarea se requiere crear un branch que lleve el estandar "nombre-apellido" y hacer pull request. El readme debe definir las tareas o pasos a ejecutar para tener el sitio corriendo en local.
